package raphaelb.rocha.infnet.edu.br.server;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.net.Socket;
import java.nio.charset.StandardCharsets;

import com.google.gson.Gson;

import raphaelb.rocha.infnet.edu.br.central.ClientCentral;
import raphaelb.rocha.infnet.edu.br.model.Requisicao;
import raphaelb.rocha.infnet.edu.br.server.interfaces.OnServerReceive;
import raphaelb.rocha.infnet.edu.br.server.util.Utils;

public class ServerReceiverListener implements OnServerReceive {
	
	@Override
	public void onAcceptSocket(Socket s) {
		System.out.println(Utils.getTimestamp()
				+ " Conex�o estabelecida com o IP " + s.getInetAddress().getHostAddress()
				+ (s.getInetAddress().isLinkLocalAddress() == true ? " (WAN)" : " (LAN)"));
		
		// Delegate to another thread
		Runnable task = () -> { onWaiting(s); };
		new Thread(task).start();
	}
	
	@Override
	public void onWaiting(Socket s) {
		String json = null;
		while (true) {
			try {
				json = null;
				// Thread espera aqui
				BufferedReader br = new BufferedReader(new InputStreamReader(s.getInputStream()));
				json = br.readLine();
				Requisicao r = onProcessJson(json);
				onProcessCommand(r);
				String resp = new ServerResponseBuilder(r).buildResponse();
				onRespondToClient(s, r, resp);
				System.out.println(Utils.getTimestamp() + " SERVER> " + json);
			} catch (Exception e) {
				breakSocket(s);
				String error = json != null ? "Erro no JSON, Servidor recebeu: " + json : e.getLocalizedMessage();
				System.err.println(Utils.getTimestamp() + " SERVER> " + error);
				break;
			}
		}
	}
	
	private void breakSocket(Socket s) {
		try {
			s.close();
		} catch (IOException e) {
			System.err.println(Utils.getTimestamp() + " SERVER> " + e.getLocalizedMessage());
		} finally {
			s = null;
		}
	}
	
	@Override
	public Requisicao onProcessJson(String json) {
		return new Gson().fromJson(json, Requisicao.class);
	}
	
	@Override
	public void onProcessCommand(Requisicao r) {
		if(r.getCmd().equalsIgnoreCase("login")) {
			ClientCentral.getInstance().addClient(r);
		} else {
			// Fazer algo
		}
	}
	
	@Override
	public void onRespondToClient(Socket s, Requisicao r, String json) {
		try {
			System.out.println(Utils.getTimestamp() + " SERVER> " + json);
			OutputStreamWriter out = new OutputStreamWriter(s.getOutputStream(), StandardCharsets.UTF_8);
			out.write(json.toString());
		} catch (Exception e) {
			System.err.println(Utils.getTimestamp() + " SERVER> " + e.getLocalizedMessage());
		}
	}
	
}
