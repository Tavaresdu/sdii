package raphaelb.rocha.infnet.edu.br.server;

import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;

import raphaelb.rocha.infnet.edu.br.server.util.Utils;

public class TcpServer {
	private int PORT;
	private ServerReceiverListener listener;
	
	public TcpServer(int p) {
		PORT = p;
	}

	public void startService() {
		System.out.println(Utils.getTimestamp() + " SERVER> Waiting for connection on port " + PORT + " (TCP)");
		Runnable task2 = () -> { receiveConenctionAsync(); };
		new Thread(task2).start();;
	}
	
	@SuppressWarnings("resource")
	private void receiveConenctionAsync() {
		while(true) {
			try {
				Socket s = new ServerSocket(PORT).accept();
				listener.onAcceptSocket(s);
			} catch (IOException e) {
				System.err.println(Utils.getTimestamp() + " SERVER> " + e.getLocalizedMessage());
				delay(2000);
			}
		}
	}
	
	private void delay(long milis) {
		try {
			Thread.sleep(milis);
		} catch (InterruptedException e) {
			System.err.println(Utils.getTimestamp() + " SERVER> " + e.getLocalizedMessage());
		}
	}

	public void setListener(ServerReceiverListener serverReceiverListener) {
		listener = serverReceiverListener;
	}

}
