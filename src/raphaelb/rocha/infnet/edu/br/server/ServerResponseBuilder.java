package raphaelb.rocha.infnet.edu.br.server;

import com.google.gson.Gson;

import raphaelb.rocha.infnet.edu.br.central.ClientCentral;
import raphaelb.rocha.infnet.edu.br.model.Requisicao;
import raphaelb.rocha.infnet.edu.br.model.Resposta;

public class ServerResponseBuilder {

	private Requisicao req;

	public ServerResponseBuilder(Requisicao r) {
		req = r;
	}

	public String buildResponse() {
		if (req.getCmd().equalsIgnoreCase("login")) {
			Resposta r = new Resposta();
			r.setId("0");
			r.setMsgNr(ClientCentral.getInstance().getDatasetSize());
			r.setData(ClientCentral.getInstance().getMessageDataset());
			return new Gson().toJson(r, Resposta.class);
		} else {
			// Fazer algo
			return null;
		}
	}

}
