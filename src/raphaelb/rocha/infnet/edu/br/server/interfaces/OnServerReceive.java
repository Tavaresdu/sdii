package raphaelb.rocha.infnet.edu.br.server.interfaces;

import java.net.Socket;

import raphaelb.rocha.infnet.edu.br.model.Requisicao;

public interface OnServerReceive {
	void onAcceptSocket(Socket s);
	void onWaiting(Socket s);
	Requisicao onProcessJson(String json);
	void onProcessCommand(Requisicao cmd);
	void onRespondToClient(Socket s, Requisicao r, String json);
}
