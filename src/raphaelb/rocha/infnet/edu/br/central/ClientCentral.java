package raphaelb.rocha.infnet.edu.br.central;

import java.util.ArrayList;
import java.util.List;

import raphaelb.rocha.infnet.edu.br.model.Client;
import raphaelb.rocha.infnet.edu.br.model.Message;
import raphaelb.rocha.infnet.edu.br.model.Requisicao;

public class ClientCentral {
	private static ClientCentral instance;
	private List<Client> cList = new ArrayList<> ();
	private List<Message> lMess = new ArrayList<>();
	private int datasetSize = 0;
	
	public void addClient(Requisicao r) {
		// Adiciona um cliente a lista de clientes online
		cList.add(new Client(r.getId(), r.getMsgNr()));
	}

	public int getDatasetSize() {
		return datasetSize; // TODO
	}

	public List<Message> getMessageDataset() {
		return lMess;
	}
	
	// Singleton
	private ClientCentral(){}
	public static ClientCentral getInstance() {
		return instance == null ? new ClientCentral() : instance;
	}
}
