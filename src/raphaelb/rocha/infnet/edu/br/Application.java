package raphaelb.rocha.infnet.edu.br;

import raphaelb.rocha.infnet.edu.br.server.ServerReceiverListener;
import raphaelb.rocha.infnet.edu.br.server.TcpServer;

public class Application {

	public static void main(String[] args) {
		TcpServer tcp = new TcpServer(3000);
		tcp.setListener(new ServerReceiverListener());
		tcp.startService();
	}

}
