package raphaelb.rocha.infnet.edu.br.model;

public class Requisicao {
	private String id;
	private String cmd;
	private int msgNr;
	
	
	public String getId() {
		return id;
	}
	public String getCmd() {
		return cmd;
	}
	public int getMsgNr() {
		return msgNr;
	}
	
}
