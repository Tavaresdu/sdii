package raphaelb.rocha.infnet.edu.br.model;

public class Message {
	private String src;
	private String data;
	
	public String getSrc() {
		return src;
	}
	public String getData() {
		return data;
	}
	
	public Message (String src, String data) {
		this.src = src;
		this.data = data;
	}
}
