package raphaelb.rocha.infnet.edu.br.model;

import java.util.List;

public class Resposta {
	private String id;
	private List<Message> data;
	private int msgNr;
	
	
	public String getId() {
		return id;
	}
	public void setData(List<Message> data) {
		this.data = data;
	}
	public void setMsgNr(int msgNr) {
		this.msgNr = msgNr;
	}
	public List<Message> getCmd() {
		return data;
	}
	public int getMsgNr() {
		return msgNr;
	}
	public void setId(String i) {
		id = i;
	}

}
