package raphaelb.rocha.infnet.edu.br.model;

import java.net.Socket;

public class Client {
	
	public Socket getS() {
		return s;
	}
	public void setS(Socket s) {
		this.s = s;
	}
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public int getMsgNr() {
		return msgNr;
	}
	public void setMsgNr(int msgNr) {
		this.msgNr = msgNr;
	}
	public Client(String id, int msgNr) {
		this.id = id;
		this.msgNr = msgNr;
	}
	
	private Socket s; // Ser[a usado para o servidor falar com ele mais tarde.
	private String id;
	private int msgNr;
}
